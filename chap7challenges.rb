#solutions to challenges from https://pine.fm/LearnToProgram/chap_07.html

#type words in, one per line, hit enter when done, get words sorted in alphabetical order
done = false
words = []
while !done
  
  input = gets.chomp
  
  if input == ""
    done = true
  else
    words.push(input)
  end
  
end
puts words.sort!()
puts

#the above, but no .sort
done = false
words = []
while !done
  
  input = gets.chomp
  
  if input == ""
    done = true
  else
    
    length = words.length
    pos = 0
    inserted = false;
    
    if words.empty?
      words.push(input)
    else
      
      for i in 0..length-1
        if input == words[i] || input < words[i]
          words.insert(i, input)
          inserted = true;
          break
        end
      end
      
      if !inserted
        words.insert(length, input)
      end
      
    end
  end
end
puts words
puts 

#table of contents printer, from an array
lineWidth = 80
toc = ["Table of Contents\n", "Chapter 1: Numbers", "page 1",
       "Chapter 2: Letters", "page 72", "Chapter 3: Variables", "page 118"]

for i in 0..toc.length-1
  if i == 0
    puts toc[i].center(lineWidth)
  elsif i % 2 != 0
    print toc[i].ljust(lineWidth/2)
  else
    puts toc[i].rjust(lineWidth/2) 
  end
end


