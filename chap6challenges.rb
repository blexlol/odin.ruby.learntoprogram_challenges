#solutions to challenges from https://pine.fm/LearnToProgram/chap_06.html

#99 bottles of beer on the wall song
bottles = 99
while bottles > 0
  puts bottles.to_s+" bottles of beer on the wall, "+bottles.to_s+" bottles of beer, you take one down, pass it around,"
  bottles -= 1
end
puts

#deaf grandma conversation
puts "Say something to grandma!"

byeCount = 0

while byeCount < 3
  
  toGrandma = gets.chomp
  
  if toGrandma == "BYE"
    byeCount += 1
    if byeCount >= 3
      puts "OKAY BYE!"
    else
    puts "WAIT DONT GO!"
    end
    
  elsif toGrandma != toGrandma.upcase
    puts "HUH?! SPEAK UP, SONNY!"
    byeCount = 0;

  else
    puts "NO, NOT SINCE 1938!"
    byeCount = 0;
  end
  
end

#leap years listing program
puts "Name two years and I'll find all the leap years between them"
puts "What's the first year?"
firstYear = gets.chomp.to_i
puts "Okay, what's the second year?"
secondYear = gets.chomp.to_i

if secondYear < firstYear
  temp = firstYear
  firstYear = secondYear
  secondYear = temp
end

counter = firstYear
leapYears = []
while counter <= secondYear
  if counter % 4 == 0 && (counter % 100 != 0 || counter % 400 == 0) 
    leapYears.push(counter)
  end
  counter += 1
end
puts leapYears.join(', ')