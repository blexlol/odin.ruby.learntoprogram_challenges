#challenges from https://pine.fm/LearnToProgram/chap_10.html

#grandfather clock that calls a blocks for each hour that has passed today
class GrandfatherClock
  
  def hoursPassed
    Time.now.hour
  end
  
  def forEachHourDo someProc
   hoursPassed.times {someProc.call}
  end
  
end

clock = GrandfatherClock.new()
bong = Proc.new{puts 'DONG!'}
clock.forEachHourDo(bong)
puts

#program logger
def log(description, &someBlock)
  puts 'Starting "'+description+'"...'
  retVal = someBlock.call.to_s
  puts '...'+description+' finished, returning: '+retVal
  retVal
end

log "outer block" do
  
  log 'inner block' do
    5
  end

  log 'another inner block' do
    'a string'
  end

end

puts

#better logger
$nestingDepth = 0

def blog(description, &someBlock)
  $nestingDepth.times{ print ' '}
  $nestingDepth += 1
  
  puts 'Starting "'+description+'"...'
  retVal = someBlock.call.to_s
  $nestingDepth -= 1
  
  $nestingDepth.times{ print ' '}
  puts '...'+description+' finished, returning: '+retVal
  retVal
end

blog "outer block" do
  
  blog 'inner block' do
    
    blog 'deepest block' do
      'how deep does this go'
    end
    
  end

  blog 'another inner block' do
    'a string'
  end

end