#solutions to challenges from https://pine.fm/LearnToProgram/chap_09.html

#birthday counter
puts "What year were you born?"
year = gets.chomp
puts "Which month were you born?"
mon = gets.chomp
puts "Which day were you born?"
day = gets.chomp

bday = Time.mktime(year,mon,day)
now = Time.new

secondsLived = now - bday

yearsLived = secondsLived/60/60/24/365
yearsLived = yearsLived.floor

puts "You're "+yearsLived.floor.to_s+" years old!"
  
yearsLived.times do
  print "SPANK! "
end

puts

#interactive orange tree

class OrangeTree
  
  def initialize 
    @height = 1
    @age = 0
    @oranges = 0
  end
  
  def countTheOranges
    puts "There are "+@oranges.to_s+" oranges on the tree"
  end
  
  def pickAnOrange
    if @oranges > 0
      puts "What a delicious orange!"
      @oranges -= 1
    else
      puts "There are no more oranges to eat this year"
    end
  end
  
  def oneYearPasses
    @age += 1
    if @age > 20
      puts "The orange tree dies ;_;"
      exit
    end
    @height += 0.3
    produceOranges
  end
  
  private
  
  def produceOranges
    @oranges = 0
    if @age > 3
      @oranges += @age*3
    end
  end
  
end

puts
myTree = OrangeTree.new()
myTree.countTheOranges
myTree.oneYearPasses
myTree.oneYearPasses
myTree.oneYearPasses
myTree.countTheOranges
myTree.pickAnOrange
myTree.pickAnOrange
myTree.oneYearPasses
myTree.oneYearPasses
myTree.oneYearPasses
myTree.countTheOranges
myTree.pickAnOrange
myTree.pickAnOrange
myTree.oneYearPasses
myTree.oneYearPasses
myTree.oneYearPasses
myTree.oneYearPasses
myTree.oneYearPasses
myTree.oneYearPasses
myTree.oneYearPasses
myTree.oneYearPasses
myTree.pickAnOrange
myTree.oneYearPasses
myTree.oneYearPasses
myTree.oneYearPasses
myTree.countTheOranges
puts

#interactive dragon, code adapted from https://pine.fm/LearnToProgram/chap_09.html

class Dragon

  def initialize name
    @name = name
    @asleep = false
    @stuffInBelly     = 10  # He's full.
    @stuffInIntestine =  0  # He doesn't need to go.
    puts @name + ' is born.'
    interact
  end

  def feed
    puts 'You feed ' + @name + '.'
    @stuffInBelly = 10
    passageOfTime
  end

  def walk
    puts 'You walk ' + @name + '.'
    @stuffInIntestine = 0
    passageOfTime
  end

  def putToBed
    puts 'You put ' + @name + ' to bed.'
    @asleep = true
    3.times do
      if @asleep
        passageOfTime
      end
      if @asleep
        puts @name + ' snores, filling the room with smoke.'
      end
    end
    if @asleep
      @asleep = false
      puts @name + ' wakes up slowly.'
    end
  end

  def toss
    puts 'You toss ' + @name + ' up into the air.'
    puts 'He giggles, which singes your eyebrows.'
    passageOfTime
  end

  def rock
    puts 'You rock ' + @name + ' gently.'
    @asleep = true
    puts 'He briefly dozes off...'
    passageOfTime
    if @asleep
      @asleep = false
      puts '...but wakes when you stop.'
    end
  end
  
  def interact
    
    while true
      input = gets.chomp

      case input
      when 'feed'
        feed
      when 'walk'
        walk
      when 'putToBed'
        putToBed
      when 'toss'
        toss
      when 'rock'
        rock
      else
        puts 'Unknown input. Available options:'
        puts 'feed'
        puts 'walk'
        puts 'putToBed'
        puts 'toss'
        puts 'rock'
      end
    end
    
  end

  private

  # "private" means that the methods defined here are
  # methods internal to the object.  (You can feed
  # your dragon, but you can't ask him if he's hungry.)

  def hungry?
    # Method names can end with "?".
    # Usually, we only do this if the method
    # returns true or false, like this:
    @stuffInBelly <= 2
  end

  def poopy?
    @stuffInIntestine >= 8
  end

  def passageOfTime
    if @stuffInBelly > 0
      # Move food from belly to intestine.
      @stuffInBelly     = @stuffInBelly     - 1
      @stuffInIntestine = @stuffInIntestine + 1
    else  # Our dragon is starving!
      if @asleep
        @asleep = false
        puts 'He wakes up suddenly!'
      end
      puts @name + ' is starving!  In desperation, he ate YOU!'
      exit  # This quits the program.
    end

    if @stuffInIntestine >= 10
      @stuffInIntestine = 0
      puts 'Whoops!  ' + @name + ' had an accident...'
    end

    if hungry?
      if @asleep
        @asleep = false
        puts 'He wakes up suddenly!'
      end
      puts @name + '\'s stomach grumbles...'
    end

    if poopy?
      if @asleep
        @asleep = false
        puts 'He wakes up suddenly!'
      end
      puts @name + ' does the potty dance...'
    end
  end

end


pet = Dragon.new 'Norbert'