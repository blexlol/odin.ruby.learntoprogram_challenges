#solutions to challenges from https://pine.fm/LearnToProgram/chap_05.html

#ask the angry boss a question, get it yelled back at you
puts "Yes? What do you want?"
request = gets.chomp
puts 'WHADDAYA MEAN "'+request.upcase+'"?!? YOU\'RE FIRED!!'
puts

#displays a justified table of contents
lineWidth = 80
puts "Table of Contents\n".center(lineWidth)
print "Chapter 1: Numbers".ljust(lineWidth/2)
puts "page 1".rjust(lineWidth/2)
print "Chapter 2: Letters".ljust(lineWidth/2)
puts "page 72".rjust(lineWidth/2)
print "Chapter 3: Variables".ljust(lineWidth/2)
puts "page 118".rjust(lineWidth/2)
