#solutions to challenges from https://pine.fm/LearnToProgram/chap_08.html

Billion = 1000000000
Million = 1000000

#english number, takes an integer and returns the number in words
def englishNumber(*args)
  
  if args.empty?
    return 'Please enter a number'
  else
    number = args[0]
  end
  
  verbose = false

  if number.is_a?(Numeric) == false || number  == ''
    return 'Please enter a number'
  end
  if number < 0  # No negative numbers.
    return 'Please enter a number that isn\'t negative.'
  end
  if number == 0
    return 'zero'
  end
  if number >= Billion*10
    return 'Enter a number less than 10 billion'
  end

  onesPlace = ['one',     'two',       'three',    'four',     'five',
               'six',     'seven',     'eight',    'nine']
  tensPlace = ['ten',     'twenty',    'thirty',   'forty',    'fifty',
               'sixty',   'seventy',   'eighty',   'ninety']
  teenagers = ['eleven',  'twelve',    'thirteen', 'fourteen', 'fifteen',
               'sixteen', 'seventeen', 'eighteen', 'nineteen']
                              
  billions  = number/Billion
  bilVal    = billions*Billion
  
  millions  = (number-bilVal)/Million
  milVal    = millions*Million
  
  thousands = (number-bilVal-milVal)/1000
  thoVal    = thousands*1000
  
  hundreds  = (number-bilVal-milVal-thoVal)/100
  hunVal    = hundreds*100
  
  tens      = (number-bilVal-milVal-thoVal-hunVal)/10
  tenVal    = tens*10
  
  ones      = number-bilVal-milVal-thoVal-hunVal-tenVal

  if verbose
    puts billions
    puts millions
    puts thousands
    puts hundreds
    puts tens
    puts ones
  end
  
  numArray = []
  
  if billions > 0
    numArray.push(englishNumber(billions)+" billion")
  end
  if millions > 0
    numArray.push(englishNumber(millions)+" million")
  end  
  if thousands > 0
    numArray.push(englishNumber(thousands)+" thousand")
  end
  if hundreds > 0
    numArray.push(englishNumber(hundreds)+" hundred")
  end
  if tens == 1 && ones > 0
    numArray.push(teenagers[ones-1])
    ones = 0
  elsif tens > 0
    numArray.push(tensPlace[tens-1]) 
  end
  if ones > 0
    numArray.push(onesPlace[ones-1])
  end
  
  return numArray.join(' ')
    
end

#boundary/equivalence tests

puts englishNumber(123419)
puts englishNumber(1)
puts englishNumber(2)
puts englishNumber(42)
puts englishNumber(9041802)
puts englishNumber(0)
puts englishNumber(10)
puts englishNumber(19)
puts englishNumber(11)
puts englishNumber(9000)
puts englishNumber(9001)
puts englishNumber(9999999999)
puts englishNumber(9999999999+1)
puts englishNumber("Not a number")
puts englishNumber()

#99 bottles of beer on the wall, using englishNumber
puts "How many bottles do you have?"

bottles = gets.chomp.to_i

while bottles > 0
  puts englishNumber(bottles).chomp+" bottles of beer on the wall, "+englishNumber(bottles).chomp+" bottles of beer, you take one down, pass it around,"
  bottles -= 1
end
